from flask import Flask,request
import base64
from keras.models import load_model
import cv2
import numpy as np

app=Flask(__name__)

face = cv2.CascadeClassifier('haar cascade files\haarcascade_frontalface_alt.xml')
leye = cv2.CascadeClassifier('haar cascade files\haarcascade_lefteye_2splits.xml')
reye = cv2.CascadeClassifier('haar cascade files\haarcascade_righteye_2splits.xml')

model = load_model('models/cnncat2.h5')
imagename="imageToSave.jpeg"

rpred=[99]
lpred=[99]

@app.route('/handle_image',methods=["GET","POST"])
def index():
    try:
        if request.method=="POST":
            # or, more concisely using with statement
            imgdata = base64.b64decode(request.form['abc'])
            with open(imagename, "wb") as f:
                f.write(imgdata)

                frame = cv2.imread(imagename)
                
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                left_eye = leye.detectMultiScale(gray)
                right_eye =  reye.detectMultiScale(gray)

                for (x,y,w,h) in right_eye:
                    r_eye=frame[y:y+h,x:x+w]
                
                    r_eye = cv2.cvtColor(r_eye,cv2.COLOR_BGR2GRAY)
                    r_eye = cv2.resize(r_eye,(24,24))
                    r_eye= r_eye/255
                    r_eye=  r_eye.reshape(24,24,-1)
                    r_eye = np.expand_dims(r_eye,axis=0)
                    rpred = model.predict_classes(r_eye)
                    break

                for (x,y,w,h) in left_eye:
                    l_eye=frame[y:y+h,x:x+w]

                    l_eye = cv2.cvtColor(l_eye,cv2.COLOR_BGR2GRAY)  
                    l_eye = cv2.resize(l_eye,(24,24))
                    l_eye= l_eye/255
                    l_eye=l_eye.reshape(24,24,-1)
                    l_eye = np.expand_dims(l_eye,axis=0)
                    lpred = model.predict_classes(l_eye)

                if(rpred[0]==0 and lpred[0]==0):
                    return 'closed'
                else:
                    return 'open'
                        
            

            
    except Exception as e:
        print(e)
        return 'error'