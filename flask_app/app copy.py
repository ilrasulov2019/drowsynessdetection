from flask import Flask,request
import base64

app=Flask(__name__)


@app.route('/pic_service',methods=["GET","POST"])
def index():
    try:
        if request.method=="POST":
            # or, more concisely using with statement
            imgdata = base64.b64decode(request.form['abc'])
            with open("imageToSave.jpeg", "wb") as f:
                f.write(imgdata)
            return 'ok'

            
    except Exception as e:
        print(e)

    
    return 'ok'