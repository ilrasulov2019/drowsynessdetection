package com.zarra.drowsydetect;

import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.io.ClassPathResource;

import java.io.IOException;

public class keras {

    public static void sdccd(){
//        // load the model
        String simpleMlp = null;
        try {
            simpleMlp = new ClassPathResource(
                    "games.h5").getFile().getPath();

        MultiLayerNetwork model = KerasModelImport.
                importKerasSequentialModelAndWeights(simpleMlp);
// make a random sample
        int inputs = 10;
        INDArray features = Nd4j.zeros(inputs);
        for (int i=0; i<inputs; i++)
            features.putScalar(new int[] {i}, Math.random() < 0.5 ? 0 : 1);
// get the prediction
        double prediction = model.output(features).getDouble(0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedKerasConfigurationException e) {
            e.printStackTrace();
        } catch (InvalidKerasConfigurationException e) {
            e.printStackTrace();
        }
    }
}
