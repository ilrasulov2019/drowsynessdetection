package com.zarra.drowsydetect;

import android.app.IntentService;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preference {
    private static Preference preferenceInstance;

    private static String serverAddress;
    private static String serverPort;
    private static String serverService;
    private static double serviceFreq;
    private static int serviceWarnOn;


    private Preference(){
    }
    public static synchronized Preference getInstance(Context context){
        if(preferenceInstance==null){
            preferenceInstance=new Preference();
        }

        SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(context);
        serverAddress=prefs.getString("connectionURL","192.168.137.1");
        serverPort=prefs.getString("connectionPort","5000");
        serverService=prefs.getString("connectionService","handle_image");
        serviceFreq= Double.parseDouble(prefs.getString("connectionFrequency","5"));
        serviceWarnOn= Integer.parseInt(prefs.getString("connectionWarnOn","10"));

        return preferenceInstance;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public String getServerPort() {
        return serverPort;
    }

    public String getServerService() {
        return serverService;
    }

    public double getServiceFreq() {
        return serviceFreq;
    }

    public int getServiceWarnOn() {
        return serviceWarnOn;
    }
}
