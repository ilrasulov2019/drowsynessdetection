package com.zarra.drowsydetect;

import android.content.Context;
import android.os.Environment;

import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;

public class Keras2 {
    public static void res(Context context,String absolutePath){
try {
//    InputStream inputStream = context.getResources().openRawResource(R.raw.cnncat2);
//    MultiLayerNetwork model = ModelSerializer.restoreMultiLayerNetwork(inputStream);
//    File file = new File("/storage/emulated/cnncat2.h5");
//    String simpleMlp = new ClassPathResource("cnncat2.h5").getFile().getPath();
    MultiLayerNetwork model = KerasModelImport.importKerasSequentialModelAndWeights("/storage/emulated/cnncat2.h5");

    int height = 28;
    int width = 28;
    int channels = 1;


    //load the image file to test
    File f=new File(absolutePath, "drawn_image.jpg");
    //Use the nativeImageLoader to convert to numerical matrix
    NativeImageLoader loader = new NativeImageLoader(height, width, channels);
    //put image into INDArray
    INDArray image = loader.asMatrix(f);
    //values need to be scaled
    DataNormalization scalar = new ImagePreProcessingScaler(0, 1);
    //then call that scalar on the image dataset
    scalar.transform(image);
    //pass through neural net and store it in output array
    INDArray output = model.output(image);
    System.out.println("a");
    }
catch (
    IOException e) {
        e.printStackTrace();
    }
catch (Exception e){
    e.printStackTrace();
}


    }
}
