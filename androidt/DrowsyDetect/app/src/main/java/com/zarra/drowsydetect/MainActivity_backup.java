package com.zarra.drowsydetect;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Base64;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class MainActivity_backup extends AppCompatActivity {

    private Camera mCamera;
    private CameraPreview mPreview;

    private Button btnSwitch;
    private Button btnStop;
    //private Button btnTakePic;
    private FrameLayout preview;

    private int score;
    private int warnOn;

    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
    MediaPlayer mp;
    Vibrator vib;
    long[] pattern = {0, 1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000, 500
            , 1000, 500, 1000, 500};


    private Boolean hasFrontCamera, hasMainCamera;

    private int cameraType=1; // 1 front, 2 back
    private int cameraId;

    private boolean serviceRunning=false;

    private String url;

    private double frequency;
    Handler handler = new Handler();
    final Runnable r = new Runnable() {
        public void run() {
            //Toast.makeText(getApplicationContext(),"Sent picture",Toast.LENGTH_SHORT).show();
            takePicture();
            handler.postDelayed(this, (long) (getFrequency()*1000));
        }
    };

    private double getFrequency(){
        return frequency;
    }



    AsyncHttpClient client = new AsyncHttpClient();
    PendingIntent pendingIntent;

    private final String TAG="CameraActivity";

    private void setButtonText(){
        if(serviceRunning)
        btnStop.setText("Stop");
        else btnStop.setText("Start");
    }

//    private void runService(Context context){
//        serviceRunning=true;
//        Intent myIntent = new Intent(context, DataService.class);
//        pendingIntent = PendingIntent.getService(context,  0, myIntent, 0);
//
//        alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.add(Calendar.SECOND, 5); // first time
//        long frequency= 5 * 1000; // in ms
//        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, calendar.getTimeInMillis(), frequency, pendingIntent);
//
//    }
//    private void stopService(){
//        if(alarmManager!=null){
//            serviceRunning=false;
//            alarmManager.cancel(pendingIntent);
//        }
//    }
    //    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
//
//        @Override
//        public void onPictureTaken(byte[] data, Camera camera) {
//
//            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
//            if (pictureFile == null){
//                Log.d(TAG, "Error creating media file, check storage permissions");
//                return;
//            }
//
//            try {
//                FileOutputStream fos = new FileOutputStream(pictureFile);
//                fos.write(data);
//                fos.close();
//            } catch (FileNotFoundException e) {
//                Log.d(TAG, "File not found: " + e.getMessage());
//            } catch (IOException e) {
//                Log.d(TAG, "Error accessing file: " + e.getMessage());
//            }
//        }
//    };

    @Override
    protected void onResume() {
        super.onResume();
        initCameraSettings();
        initCamera();

        mp = MediaPlayer.create (getApplicationContext(), alarmSound);
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        setupConnectionProperties();
        setButtonText();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupConnectionProperties();



        preview = (FrameLayout) findViewById(R.id.camera_preview);

        btnSwitch=findViewById(R.id.btnSwitch);

        btnSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cameraType==1){
                    cameraType=2;
                }
                else {
                    cameraType=1;
                }
                initCameraSettings();
                if(!hasMainCamera && cameraType==2){
                    Toast.makeText(MainActivity_backup.this,"No Main camera",Toast.LENGTH_SHORT).show();
                }
                else if(!hasFrontCamera && cameraType==1){
                    Toast.makeText(MainActivity_backup.this,"No Front camera",Toast.LENGTH_SHORT).show();
                }
                else {
                cameraId = getCameraId(cameraType);
                initCamera();
                }
            }
        });


        btnStop=findViewById(R.id.btnStop);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(serviceRunning){
                    stopServic();
                } else{
                    runService();
                }
                setButtonText();
            }
        });
        //btnTakePic=findViewById(R.id.btnTakePic);

//        btnTakePic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Toast.makeText(MainActivity.this,"Taken",Toast.LENGTH_SHORT).show();
//                takePicture();
//            }
//        });



//        btnTakePic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // get an image from the camera
//                mCamera.takePicture(null, null, mPicture);
//            }
//        });

        initCameraSettings();
        initCamera();


        runService();
        setButtonText();

    }

    private void setupConnectionProperties() {
        Preference preference=Preference.getInstance(MainActivity_backup.this);
        url= "http://" + preference.getServerAddress() + ":" + preference.getServerPort() + "/"+preference.getServerService();
        frequency=preference.getServiceFreq();
        warnOn=preference.getServiceWarnOn();
    }

    private void takePicture() {
        mCamera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                //Toast.makeText(MainActivity.this,"Aachen",Toast.LENGTH_SHORT).show();


               // data=rotate(data);
                String encodedImage = Base64.encodeToString(data, Base64.DEFAULT);
                RequestParams params=new RequestParams();
                params.put("abc",encodedImage);
                client.post(url,params, new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        // called before request is started
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                        // called when response HTTP status is "200 OK"
                        //Toast.makeText(MainActivity.this,new String(response),Toast.LENGTH_SHORT).show();
                        String responseText=new String(response);

                        if(responseText.equals("open")){
                            //Log.d("SCORE","open");
                            score=score-1;
                            if(score<0)
                                score=0;
                        }
                        else if(responseText.equals("closed")){
                            //Log.d("SCORE","closed");
                            score=score+1;
                        }
                        //Log.d("SCORE",responseText);
                        Log.d("SCORE","Score is "+score);
                        reactToScore();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    }

                    @Override
                    public void onRetry(int retryNo) {
                        // called when request is retried
                    }
                });
            }
        });
    }

    private void reactToScore(){
        Log.d("SCORE","warn on "+warnOn);
        if(score>=warnOn && !mp.isPlaying()){

            mp.setVolume(75,75);
            mp.start();
            vib.vibrate(pattern,-1);

        }
    }

    private void stopServic(){
        serviceRunning=false;
        if(mp!=null && mp.isPlaying()){
            mp.stop();
            mp.reset();
            mp = MediaPlayer.create (getApplicationContext(), alarmSound);}
        vib.cancel();


        handler.removeCallbacksAndMessages(null);
    }
    private void runService(){
        serviceRunning=true;
        score=0;
        handler.postDelayed(r, (long) (frequency*1000));
    }
    private byte[] rotate(byte[] data){
        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);

        File f3=new File(Environment.getExternalStorageDirectory()+"/inpaint/");
        if(!f3.exists())
            f3.mkdirs();
        OutputStream outStream = null;
        File file = new File(Environment.getExternalStorageDirectory() + "/inpaint/"+"seconds"+".jpeg");
        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.close();



        ExifInterface ei = new ExifInterface(file.getAbsolutePath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                bmp = rotateImage(bmp, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                bmp = rotateImage(bmp, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                bmp = rotateImage(bmp, 270);
                break;

        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            file.delete();
        return stream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return data;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    private void initCameraSettings(){
        if(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){

            cameraId = getCameraId(cameraType);

            if(cameraId != -1){
                if(cameraType==1)
                    hasFrontCamera = true;
                else if(cameraType==2)
                    hasMainCamera = true;
            }else{
                hasFrontCamera = true;
                hasMainCamera = true;
            }
        }else{
            hasFrontCamera = true;
            hasMainCamera = true;
        }}


    private void initCamera(){
        if(Build.VERSION.SDK_INT>=23 &&
                (ActivityCompat.checkSelfPermission(MainActivity_backup.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(MainActivity_backup.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED))
            requestPermissions(new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},3000);
            //setCameraDisplayOrientation(MainActivity.this,mCamera.);
        else
        {
            // Create an instance of Camera
            mCamera = getCameraInstance(cameraId);

            // Create our Preview view and set it as the content of our activity.
            mPreview = new CameraPreview(this, mCamera);
            preview.addView(mPreview);
            setCameraDisplayOrientation(MainActivity_backup.this,cameraId,mCamera);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean finishActivity=true;
        if(requestCode==3000){
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED
            && grantResults[1]==PackageManager.PERMISSION_GRANTED){
              finishActivity=false;
            }
        }
        if(finishActivity)
            finish();
        else
            initCamera();
    }

    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, Camera camera) {

        Camera.CameraInfo info =
                new Camera.CameraInfo();

        Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        camera.setDisplayOrientation(result);
    }

    private int getCameraId(int type){
        int camId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo ci = new Camera.CameraInfo();

        int cameraTypeL=Camera.CameraInfo.CAMERA_FACING_FRONT;

        if(type==1)
            cameraTypeL=Camera.CameraInfo.CAMERA_FACING_FRONT;
        else if(type==2)
            cameraTypeL=Camera.CameraInfo.CAMERA_FACING_BACK;

        for(int i = 0;i < numberOfCameras;i++){
            Camera.getCameraInfo(i,ci);
            if(ci.facing == cameraTypeL){
                camId = i;
            }
        }

        return camId;
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /** A safe way to get an instance of the Camera object. */
    public Camera getCameraInstance(int cameraId){
        Camera c = null;
        releaseCamera();
        try {
            c = Camera.open(cameraId); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();              // release the camera immediately on pause event
        if(mp.isPlaying())
            mp.stop();
        if(serviceRunning){
            stopServic();
        }
        setButtonText();
    }



    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    public void onSetiingsClick(View view) {
        Intent intent=new Intent(MainActivity_backup.this,PrefsActivity.class);
        stopServic();
        startActivity(intent);
    }



}